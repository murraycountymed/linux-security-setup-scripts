#!/bin/bash
# This script cleans up errors when attempting to run updates with
# out of date keys, resulting in errors like the following:
# error: failed to commit transaction (invalid or corrupted package (PGP signature))
# Errors occurred, no packages were upgraded.

sudo rm -R /etc/pacman.d/gnupg/
sudo pacman-key --init
sudo pacman-key --populate
sudo pacman -Syyu
