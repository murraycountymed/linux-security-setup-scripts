#! /bin/bash

wget http://intranet.murraycountymed.org/Sentinel.deb
sudo apt install ./Sentinel.deb
echo "Please copy/paste the SentinelOne Site Token Here:"
read token
sudo sentinelctl management token set $token
echo "Starting the agent..."
sudo sentinelctl control start
echo "Verifying agent status..."
sudo sentinelctl control status
