import os
from pathlib import Path
import shutil
import json


with open(os.path.dirname(os.path.realpath(__file__)) + "/config.json", "r") as jsonfile:
    config = json.load(jsonfile)


def get_directories(path: Path):
    directories = []
    for file in path.glob("*"):
        if file.is_dir():
            directories.append(file)
    return directories


def newest_file(path: Path, pattern: str = "*"):
    try:
        files = path.glob(pattern)
        return max(files, key=lambda x: x.stat().st_ctime)
    except ValueError:
        return None


patterns = config["patterns"]
path = Path(config["src_path"])
copy_path = Path(config["dst_path"])

for file in patterns:
    latest = newest_file(path, file)
    if latest is not None:
        print(f"Match found    : '{file}'")
        print(f"Newest match   : '{latest.name}'")
    new_file_name = file.replace("*", "-LATEST")
    new_file = copy_path / new_file_name
    if latest is not None:
        src_filesize = os.path.getsize(latest)
        if not new_file.exists():
            print("No matching file in destination, copying new file...")
            shutil.copy(latest, new_file)
        else:
            dst_filesize = os.path.getsize(new_file)
            if src_filesize != dst_filesize:
                print(f"Source filesize = {src_filesize} / Filesize in destination = {dst_filesize}")
                print("Copying new file to destination")
                shutil.copy(latest, new_file)
            else:
                print("Source and destination filesizes match, not copying!")
