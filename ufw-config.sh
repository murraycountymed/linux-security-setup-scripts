#!/bin/bash

ufw allow 80/tcp comment "accept http"
ufw allow 443/tcp comment "accept https"
ufw allow from 192.168.1.132 proto tcp to any port 10050 comment "accept Zabbix Agent"
ufw allow from 192.168.0.0/21 proto tcp to any port 22 comment "accept local ssh"
ufw allow from 192.168.0.0/21 proto tcp to any port 8384 comment "syncthing web"
ufw allow from 192.168.0.0/21 proto tcp to any port 22000 comment "syncthing xfer"
ufw allow from 192.168.0.0/21 proto udp to any port 22000 comment "syncthing xfer"
ufw allow from 192.168.0.0/21 proto udp to any port 21027 comment "syncthing discovery"
